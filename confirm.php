<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="design.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    
</head>

<body>

<form action="" onsubmit="return validate()" method="POST">

<div class="item_header item_header1">
    
    <!-- Hiển thị lỗi  -->
    <div class="header">

        <!-- div chọn họ và tên -->
        <div class="item">

            <div class="item_left">
                Họ và tên
            </div>

            <div class="item_right_1">
                <?php echo $_SESSION['fullname'];?>

            </div>
            
        </div>

        <!--  div chọn giới tính  -->
        <div class="item">

            <div class="item_left">
                Giới tính
            </div>

            <div class="item_right_1">
                <?php echo $_SESSION['gender1'];?>
                
            </div>
            <!-- dùng mang đe luu thông tin giới tính, dùng for đê hien thị thông tin giới tính  --> 
        </div>


        <!--  div phân khoa -->
        <div class="item">

            <div class="item_left">
                Phân khoa
            </div>

            <div class="item_right_1">
                <?php echo $_SESSION['select_khoa'];?>
            </div>
            
        </div>

        <!-- div chọn ngày sinh -->
        <div class="item">

            <div class="item_left">
                Ngày sinh
            </div>

            <div class="item_right_1">
                <?php echo $_SESSION['birthday'];?>
            </div>
        </div>


        <!-- div chọn địa chỉ -->
        <div class="item">

            <div class="item_left">
                Địa chỉ
            </div>

            <div class="item_right_1">
                <?php echo $_SESSION['address'];?>
            </div>

        </div>

        <div class="item">

            <div class="item_left">
                Hình ảnh
            </div>
            <div class="image">
                <img class="image_1" src="
                <?php echo $_SESSION['fileupload'];?> 
                " alt=""> 

            </div>
            
            
        </div>

        <!-- button đăng ký -->
        <button class="button_submit submit_1" name="submit" >
            Đăng ký
        </button>
    </div>

</div>
</form>
</body>
</html>
